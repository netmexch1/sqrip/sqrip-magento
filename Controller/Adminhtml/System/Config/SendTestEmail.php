<?php

namespace Sqrip\CustomPayment\Controller\Adminhtml\System\Config;

class SendTestEmail extends \Magento\Backend\App\Action
{
    protected $orderSender;

    protected $storeManager;

    protected $scopeConfig;

    private $logger;

    private $client;

    protected $format;

    protected $language;

    protected $countryCode;

    protected $orderCollectionFactory;

    protected $jsonFactory;

    public function __construct(
        \Sqrip\CustomPayment\Model\Mail\Sender\OrderSender         $orderSender,
        \Magento\Store\Model\StoreManagerInterface                 $storeManager,
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Backend\App\Action\Context                        $context,
        \Magento\Framework\App\Config\ScopeConfigInterface         $scopeConfig,
        \GuzzleHttp\Client                                         $client,
        \Sqrip\CustomPayment\Model\Config\Source\Format            $format,
        \Sqrip\CustomPayment\Model\Config\Source\Language          $language,
        \Sqrip\CustomPayment\Model\Config\Source\CountryCode       $countryCode,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Controller\Result\JsonFactory           $jsonFactory
    )
    {
        parent::__construct($context);

        $this->orderSender = $orderSender;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->client = $client;
        $this->format = $format;
        $this->language = $language;
        $this->countryCode = $countryCode;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->jsonFactory = $jsonFactory;
    }

    public function execute()
    {
        $this->logger->debug('SendTestEmail->execute');

        $currencyCode = $this->storeManager->getStore()->getCurrentCurrency()->getCode();

        $dueDate = strtotime(date('Y-m-d') . " + " . $this->getScopeConfigValue('maturity') . " days");

        $format = $this->format->toArray()[$this->getScopeConfigValue('format')];
        $countryCode = $this->countryCode->toArray()[$this->getScopeConfigValue('third_address_country_code')];
        $language = $this->language->toArray()[$this->getScopeConfigValue('language')];

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key')
        ];
        $body = [
            "iban" => [
                "iban" => $this->getScopeConfigValue('address_qr_iban')
            ],
            "payable_by" => [
                "name" => "netmex digital gmbh",
                "street" => "Laurenzenvorstazxer7dt 11",
                "postal_code" => 5000,
                "town" => "Aarau",
                "country_code" => "CH"
            ],
            "payable_to" => [
                "name" => $this->getScopeConfigValue('third_address_name'),
                "street" => $this->getScopeConfigValue('third_address_street'),
                "postal_code" => $this->getScopeConfigValue('third_address_zipcode'),
                "town" => $this->getScopeConfigValue('third_address_city'),
                "country_code" => "$countryCode"
            ],
            "payment_information" => [
                "currency_symbol" => "$currencyCode",
                "amount" => 10,
                "message" => $this->buildAdditionalInformation($this->getScopeConfigValue('additional_information'), $dueDate),
                "due_date" => date('Y-m-d', $dueDate),
                "reference_is_set" => true
            ],
            "due_date" => date('Y-m-d', $dueDate),
            "lang" => "$language",
            "file_type" => "pdf",
            "product" => "$format",
            "source" => "magento"
        ];

        if ($this->getScopeConfigValue('qr_code_basis') == 1) {
            $body['payment_information']['qr_reference'] = "1";
        }
        if ($this->getScopeConfigValue('initiate_qr_ref')) {
            $body['payment_information']['initial_digits'] = $this->getScopeConfigValue('initiate_qr_ref');
        }
        if ($this->getScopeConfigValue('payer') == 1) {
            $body['payable_by']['name'] = "Sophie Mustermann\nnetmex digital gmbh";
        }
        if (strpos(strtolower($this->getScopeConfigValue('additional_information')), 'due_date format') !== false) {
            unset($body['payment_information']['due_date']);
        }

        $this->logger->debug('body', $body);

        $request = new \GuzzleHttp\Psr7\Request('POST', 'https://beta.sqrip.ch/api/code', $headers, json_encode($body));
        $response = $this->client->send($request);

        $response = json_decode((string)$response->getBody(), true);

        $fileUrl = $this->storeFile($response['pdf_file']);

        $this->logger->debug('$fileUrl');
        $this->logger->debug($fileUrl);

        $order = $this->_objectManager->create(\Magento\Sales\Model\Order::class);
        $store = $this->_objectManager->create(\Magento\Store\Model\Store::class);
        $store->setStoreId(1);
        $order->setIncrementId('000001');

        $customerEmail = $this->getRequest()->getParam('customer_email');
        if (empty($customerEmail)) {
            $customerEmail = $this->getScopeConfigValue('send_test_email_to');
            if (empty($customerEmail)) {
                $customerEmail = $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            }
        }
        $order->setCustomerEmail("$customerEmail");
        $order->setCustomerName('Administrator');
        $store->setStore($store);

        $this->orderSender->send($order);

        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        return $resultJson->setData([
            'success' => true,
            'message' => $fileUrl
        ]);
    }

    private function buildAdditionalInformation($additionalInformation, $dueDate)
    {
        $this->logger->debug('SqripApi->buildAdditionalInformation');

        $dateShortcodes = [];

        preg_match_all('/\[due_date format="(.*)"\]/', $additionalInformation, $dateShortcodes);
        foreach ($dateShortcodes[0] as $index => $dateShortcode) {
            $format = $dateShortcodes[1][$index];

            $dueDateFormat = ucwords(\IntlDateFormatter::formatObject(\DateTime::createFromFormat('U', $dueDate), $format));
            if (!$dueDateFormat) {
                continue;
            }

            $additionalInformation = str_replace($dateShortcode, $dueDateFormat, $additionalInformation);
        }

        return str_replace('[order_number]', '000001', $additionalInformation);
    }

    private function storeFile($fileUrl)
    {
        $this->logger->debug('SqripApi->storeFile');

        $response = $this->client->get(
            $fileUrl,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]
        );

        $shopName = $this->scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $orderDate = date('Ymd');

        $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
        $fileName = str_replace("[order_number]", '000001', $fileName);
        $fileName = str_replace("[order_date]", $orderDate, $fileName);
        $fileName = str_replace("[shop_name]", $shopName, $fileName);
        $fileName = str_replace(" ", "-", $fileName);

        $filesystem = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\Filesystem');
        $file = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->openFile($fileName, 'w+');
        $file->lock();
        $file->write($response->getBody());
        $file->unlock();
        $file->close();

        $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');

        return $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $fileName;
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
