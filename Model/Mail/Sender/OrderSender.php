<?php

namespace Sqrip\CustomPayment\Model\Mail\Sender;

class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{
    protected $scopeConfig;

    protected $templateContainer;

    protected $transportBuilder;

    protected $directoryList;

    protected $_designerhelper;

    protected $templateFactory;

    private $loggerInterface;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface       $scopeConfig,
        \Magento\Framework\Mail\Template\Factory                 $templateFactory,
        \Magento\Framework\App\Filesystem\DirectoryList          $directoryList,
        \Magento\Framework\Mail\Template\TransportBuilder        $transportBuilder,
        \Magento\Sales\Model\Order\Email\Container\Template      $templateContainer,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory    $senderBuilderFactory,
        \Psr\Log\LoggerInterface                                 $logger,
        \Magento\Sales\Model\Order\Address\Renderer              $addressRenderer,
        \Magento\Payment\Helper\Data                             $paymentHelper,
        \Magento\Sales\Model\ResourceModel\Order                 $orderResource,
        \Magento\Framework\App\Config\ScopeConfigInterface       $globalConfig,
        \Magento\Framework\Event\ManagerInterface                $eventManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->templateFactory = $templateFactory;
        $this->templateContainer = $templateContainer;
        $this->loggerInterface = $logger;
        $this->transportBuilder = $transportBuilder;
        $this->directoryList = $directoryList;

        parent::__construct(
            $this->templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $orderResource,
            $globalConfig,
            $eventManager
        );
    }

    public function send(\Magento\Sales\Model\Order $order, $forceSyncMode = false)
    {
        if ($this->getScopeConfigValue('suppress_qr_generation') == 0 && $this->getScopeConfigValue('active') == 1) {
            $this->loggerInterface->debug('OrderSender->send');

            $orderId = $order->getIncrementId();
            $storeId = $order->getStore()->getStoreId();

            $shopName = $this->scopeConfig->getValue(
                'general/store_information/name',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $orderDate = date('Ymd');

            $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
            $fileName = str_replace("[order_number]", $orderId, $fileName);
            $fileName = str_replace("[order_date]", $orderDate, $fileName);
            $fileName = str_replace("[shop_name]", $shopName, $fileName);
            $fileName = str_replace(" ", "-", $fileName);

            $mediaDirectory = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $filePath = "$mediaDirectory/$fileName";
            $fileContents = file_get_contents($filePath);

            $attachment = new \Laminas\Mime\Part($fileContents);
            $attachment->type = \Laminas\Mime\Mime::MULTIPART_ALTERNATIVE;
            $attachment->filename = $fileName;
            $attachment->disposition = \Laminas\Mime\Mime::DISPOSITION_ATTACHMENT;
            $attachment->encoding = \Laminas\Mime\Mime::ENCODING_BASE64;

            $html = new \Laminas\Mime\Part("<h4>QR Invoice PDF for Order $orderId</h4>");
            $html->setType(\Laminas\Mime\Mime::TYPE_HTML);

            $body = new \Laminas\Mime\Message();
            $body->setParts([$html, $attachment]);

            try {
                $transport = $this->transportBuilder
                    ->setTemplateIdentifier('sales_email_order_template')
                    ->setTemplateOptions([
                        'area' => 'frontend',
                        'store' => $storeId,
                    ])
                    ->setTemplateVars([
                        'order' => $order,
                    ])
                    ->setFrom($this->identityContainer->getEmailIdentity())
                    ->addTo($order->getCustomerEmail(), $order->getCustomerName())
                    ->getTransport();

                $transport->getMessage()->setBody($body);

                if ($transport) {
                    $transport->sendMessage();

                    return true;
                }
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
        return parent::send($order, $forceSyncMode);
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
