<?php

namespace Sqrip\CustomPayment\Model;

class PaymentMethod extends \Magento\Payment\Model\Method\AbstractMethod
{
    protected $_code = 'custompayment';

    protected $scopeConfig;

    protected $sqripApi;

    protected $loggerInterface;

    protected $_isGateway = true;

    protected $_isOffline = true;

    protected $_canOrder = true;

    protected $_canAuthorize = true;

    protected $_canCapture = true;

    protected $_canCapturePartial = true;

    public function __construct(
        \Sqrip\CustomPayment\Model\Api\SqripApi                 $sqripApi,
        \Psr\Log\LoggerInterface                                $loggerInterface,
        \Magento\Framework\Model\Context                        $context,
        \Magento\Framework\Registry                             $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory       $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory            $customAttributeFactory,
        \Magento\Payment\Helper\Data                            $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface      $scopeConfig,
        \Magento\Payment\Model\Method\Logger                    $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection = null,
        array                                                   $data = []
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->sqripApi = $sqripApi;
        $this->loggerInterface = $loggerInterface;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        return parent::isAvailable($quote);
    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->loggerInterface->debug('SqripApi->sendRequest:authorize');

        $payment->setIsTransactionPending(true);
        $payment->setIsTransactionClosed(false);
        $payment->capture();
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->loggerInterface->debug('SqripApi->sendRequest:capture');

        if ($this->getScopeConfigValue('suppress_qr_generation') == 0 && empty($payment->getOrder()->getState())) {
            $this->sqripApi->generate($payment, $amount);
        }

        return $this;
    }

    public function getTitle()
    {
        $this->loggerInterface->debug('SqripApi->sendRequest:getTitle');

        return $this->getScopeConfigValue('title');
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
