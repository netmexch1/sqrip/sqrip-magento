<?php

namespace Sqrip\CustomPayment\Model\Api;

class SqripApi
{
    protected $storeManager;

    private $client;

    private $logger;

    protected $scopeConfig;

    protected $customerSession;

    protected $orderFactory;

    protected $format;

    protected $language;

    protected $countryCode;

    public function __construct(\Magento\Store\Model\StoreManagerInterface                 $storeManager,
                                \GuzzleHttp\Client                                         $client,
                                \Psr\Log\LoggerInterface                                   $logger,
                                \Magento\Framework\App\Config\ScopeConfigInterface         $scopeConfig,
                                \Magento\Customer\Model\Session                            $customerSession,
                                \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderFactory,
                                \Sqrip\CustomPayment\Model\Config\Source\Format            $format,
                                \Sqrip\CustomPayment\Model\Config\Source\Language          $language,
                                \Sqrip\CustomPayment\Model\Config\Source\CountryCode       $countryCode)
    {
        $this->storeManager = $storeManager;
        $this->client = $client;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->orderFactory = $orderFactory;
        $this->format = $format;
        $this->language = $language;
        $this->countryCode = $countryCode;
    }

    public function generate(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->logger->debug('SqripApi->sendRequest');

        try {
            $customer = $this->customerSession->getCustomer();

            $shippingAddress = $customer->getDefaultShippingAddress();
            $shippingName = $shippingAddress->getName();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingCity = $shippingAddress->getCity();
            $shippingPostcode = $shippingAddress->getPostcode();
            $shippingCountryId = $shippingAddress->getCountryId();
            $shippingCompanyName = $shippingAddress->getCompany();

            $currencyCode = $this->storeManager->getStore()->getCurrentCurrency()->getCode();

            $dueDate = strtotime(date('Y-m-d') . " + " . $this->getScopeConfigValue('maturity') . " days");

            $format = $this->format->toArray()[$this->getScopeConfigValue('format')];
            $countryCode = $this->countryCode->toArray()[$this->getScopeConfigValue('third_address_country_code')];
            $language = $this->language->toArray()[$this->getScopeConfigValue('language')];

            $orderId = $payment->getOrder()->getIncrementId();

            $initialDigits = $orderId;

            $headers = [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key')
            ];
            $body = [
                "iban" => [
                    "iban" => $this->getScopeConfigValue('address_qr_iban')
                ],
                "payable_by" => [
                    "name" => $shippingCompanyName ?? $shippingName,
                    "street" => $shippingStreet[0],
                    "postal_code" => $shippingPostcode,
                    "town" => $shippingCity,
                    "country_code" => $shippingCountryId
                ],
                "payable_to" => [
                    "name" => $this->getScopeConfigValue('third_address_name'),
                    "street" => $this->getScopeConfigValue('third_address_street'),
                    "postal_code" => $this->getScopeConfigValue('third_address_zipcode'),
                    "town" => $this->getScopeConfigValue('third_address_city'),
                    "country_code" => "$countryCode"
                ],
                "payment_information" => [
                    "currency_symbol" => "$currencyCode",
                    "amount" => $amount,
                    "message" => $this->buildAdditionalInformation($this->getScopeConfigValue('additional_information'),
                        $dueDate,
                        $orderId),
                    "due_date" => date('Y-m-d', $dueDate),
                    "reference_is_set" => true
                ],
                "due_date" => date('Y-m-d', $dueDate),
                "lang" => "$language",
                "file_type" => "pdf",
                "product" => "$format",
                "source" => "magento"
            ];

            if ($this->getScopeConfigValue('qr_code_basis') == 1) {
                $body['payment_information']['qr_reference'] = $initialDigits;
            }
            if ($this->getScopeConfigValue('initiate_qr_ref')) {
                $body['payment_information']['initial_digits'] = $this->getScopeConfigValue('initiate_qr_ref');
            }
            if ($this->getScopeConfigValue('payer') == 1) {
                $body['payable_by']['name'] = "$shippingName\n$shippingCompanyName";
            }
            if (strpos(strtolower($this->getScopeConfigValue('additional_information')), 'due_date format') !== false) {
                unset($body['payment_information']['due_date']);
            }

            $this->logger->debug('body', $body);

            $request = new \GuzzleHttp\Psr7\Request('POST', 'https://beta.sqrip.ch/api/code', $headers, json_encode($body));
            $response = $this->client->send($request);

            $response = json_decode((string)$response->getBody(), true);

            $fileUrl = $this->storeFile($response['pdf_file'], $orderId);

            $this->logger->debug('$fileUrl');
            $this->logger->debug($fileUrl);

            return $fileUrl;
        } catch (\Exception $exception) {
            $this->logger->debug('$exception');
            $this->logger->debug($exception);
        }

        return '';
    }

    private function buildAdditionalInformation($additionalInformation, $dueDate, $orderNumber)
    {
        $this->logger->debug('SqripApi->buildAdditionalInformation');

        $dateShortcodes = [];

        preg_match_all('/\[due_date format="(.*)"\]/', $additionalInformation, $dateShortcodes);
        foreach ($dateShortcodes[0] as $index => $dateShortcode) {
            $format = $dateShortcodes[1][$index];

            $dueDateFormat = ucwords(\IntlDateFormatter::formatObject(\DateTime::createFromFormat('U', $dueDate), $format));
            if (!$dueDateFormat) {
                continue;
            }

            $additionalInformation = str_replace($dateShortcode, $dueDateFormat, $additionalInformation);
        }

        return str_replace("[order_number]", $orderNumber, $additionalInformation);
    }

    private function storeFile($fileUrl, $orderId)
    {
        $this->logger->debug('SqripApi->storeFile');

        $response = $this->client->get(
            $fileUrl,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]
        );

        $shopName = $this->scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $orderDate = date('Ymd');

        $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
        $fileName = str_replace("[order_number]", $orderId, $fileName);
        $fileName = str_replace("[order_date]", $orderDate, $fileName);
        $fileName = str_replace("[shop_name]", $shopName, $fileName);
        $fileName = str_replace(" ", "-", $fileName);

        $filesystem = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\Filesystem');
        $file = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->openFile($fileName, 'w+');
        $file->lock();
        $file->write($response->getBody());
        $file->unlock();
        $file->close();

        $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');

        return $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $fileName;
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
