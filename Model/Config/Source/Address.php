<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Address implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 2, 'label' => __('Third address')], ['value' => 1, 'label' => __('from Magento')], ['value' => 0, 'label' => __('from sqrip account')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('from sqrip account'), 1 => __('from Magento'), 2 => __('Third address')];
    }
}
