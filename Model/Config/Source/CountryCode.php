<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class CountryCode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 5, 'label' => __('Switzerland')], ['value' => 4, 'label' => __('Liechtenstein')],
            ['value' => 3, 'label' => __('Italy')], ['value' => 2, 'label' => __('Germany')],
            ['value' => 1, 'label' => __('France')], ['value' => 0, 'label' => __('Austria')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('AT'), 1 => __('FR'),
            2 => __('DE'), 3 => __('IT'),
            4 => __('LI'), 5 => __('CH')];
    }
}
