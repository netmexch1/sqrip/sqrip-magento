<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Language implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 3, 'label' => __('German')], ['value' => 2, 'label' => __('French')],
            ['value' => 1, 'label' => __('Italian')], ['value' => 0, 'label' => __('English')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('en'), 1 => __('it'),
            2 => __('fr'), 3 => __('de')];
    }
}
