<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class Payer implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Both Company Name and First/Last name')], ['value' => 0, 'label' => __('Either Company Name(priority) or First/Last name')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('Either Company Name(priority) or First/Last name'), 1 => __('Both Company Name and First/Last name')];
    }
}
