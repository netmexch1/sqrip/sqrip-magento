<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class HandleValidationScript extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $shopName = $this->_scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return '
        <script>
        require(["jquery", "select2"], function($, select2){
            jQuery("[id*=handle_validation_script]").hide();

            const submitButton = jQuery("button#save");
            const initiateQRRef = jQuery("input[id*=initiate_qr_ref]");
            const additionalInformation = jQuery("textarea[id*=additional_information]");
            const fileName = jQuery("textarea[id*=file_name]");
            const shopName = "' . $shopName . '";

            initiateQRRef.on("input", function() {
                const inputVal = jQuery(this).val();

                if (jQuery(this).hasClass("simple-iban")) {
                    if (!/^(|.{6})$/.test(inputVal)) {
                        jQuery(this).addClass("sqrip-rounded-red");
                    } else {
                        jQuery(this).removeClass("sqrip-rounded-red");
                    }
                }
                if (jQuery(this).hasClass("qr-iban")) {
                    if (!/^(|\d{6})$/.test(inputVal)) {
                        jQuery(this).addClass("sqrip-rounded-red");
                    } else {
                        jQuery(this).removeClass("sqrip-rounded-red");
                    }
                }

                validate_form();
            });

            additionalInformation.on("input", function() {
                let inputVal = jQuery(this).val();
                const orderDate = "06. September 2022";
                const orderNumber = "000001";

                inputVal = inputVal.replace(/\[due_date format=".*?"]/, orderDate);
                inputVal = inputVal.replace("[order_number]", orderNumber);

                if (inputVal.length >= 140 || (inputVal.match(/\n/g) || []).length > 4) {
                    jQuery(this).addClass("sqrip-rounded-red");
                } else {
                    jQuery(this).removeClass("sqrip-rounded-red");
                }

                validate_form();
            });

            fileName.on("input", function() {
                let inputVal = jQuery(this).val();
                const orderDate = new Date().toISOString().split("T")[0];
                const orderNumber = "000001";

                inputVal = inputVal.replace("[order_date]", orderDate);
                inputVal = inputVal.replace("[order_number]", orderNumber);
                inputVal = inputVal.replace("[shop_name]", shopName);
                inputVal = inputVal.replace(/ /g, "-");

                if (!/^([\w-]+)(?=\.[\w]+$)/.test(`${inputVal}.pdf`)) {
                    jQuery(this).addClass("sqrip-rounded-red");
                } else {
                    jQuery(this).removeClass("sqrip-rounded-red");
                }

                validate_form();
            });

            function validate_form() {
                if (jQuery(".sqrip-rounded-red")[0]) {
                    submitButton.attr("disabled", true);
                } else {
                    submitButton.attr("disabled", false);
                }
            }

            jQuery("select[id*=delete_qr_invoice_after_status]").select2({
                allowClear: true,
                placeholder: ""
            });
        });
        </script>
        ';
    }
}
