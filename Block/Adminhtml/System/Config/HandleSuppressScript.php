<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class HandleSuppressScript extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <script>
        require(["jquery"], function($){
            jQuery("[id*=handle_suppress_script]").hide();

            const suppressQRDownload = jQuery("select[id*=suppress_qr_generation]");
            const offerQRDownload = jQuery("[id*=offer_qr_download]");

            handleOfferDownload(suppressQRDownload.val());

            suppressQRDownload.on("change", function() {
                handleOfferDownload(jQuery(this).val());
            });

            function handleOfferDownload(selectedValue){
                if(selectedValue !== "0"){
                    offerQRDownload.hide();
                }
                else{
                    offerQRDownload.show();
                }
            }
        });
        </script>
        ';
    }
}
