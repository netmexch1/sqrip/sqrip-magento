<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class TestConnectionButton extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <button id="connection-test" type="button">' . __('Connection test') . '
        </button>
        <div class="sqrip-api-notice mt-10 updated">
            <p>' . __('Valid, active API Key') . '</p>
        </div>
        <div class="sqrip-api-error-notice mt-10 updated">
            <p>' . __('Invalid API Key') . '</p>
        </div>
        <script>
        require(["jquery"], function($){
            const apiKey = jQuery("textarea[id*=api_key]");
            const sqripNotice = jQuery("div[class*=sqrip-api-notice]");
            const sqripErrorNotice = jQuery("div[class*=sqrip-api-error-notice]");

            apiKey.on("input", function(){
                sqripNotice.hide();
                sqripErrorNotice.hide();
            });

            jQuery("#connection-test").on("click", function(){
                const apiKeyValue = apiKey.val();

                jQuery.ajax({
                    method : "GET",
                    url : "https://beta.sqrip.ch/api/details",
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + apiKeyValue
                    },
                    showLoader: true,
                    success: function(response) {
                        if(response) {
                            sqripNotice.show();
                            sqripErrorNotice.hide();
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ){
                        if(errorThrown){
                            sqripNotice.hide();
                            sqripErrorNotice.show();
                        }
                    }
                });
            });
        });
        </script>
        ';
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
