<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\Order\Creditmemo;

class RefundButton extends \Magento\Backend\Block\Template
{

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array                                   $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function getButton()
    {
        return '<button id="sqrip-refund-button">' . __('Refund using sqrip') . '</button><input id="sqrip-refund-input" type="hidden" name="sqrip_refund" value="true">';
    }
}
