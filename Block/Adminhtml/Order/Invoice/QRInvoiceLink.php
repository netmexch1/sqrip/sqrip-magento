<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\Order\Invoice;

class QRInvoiceLink extends \Magento\Framework\View\Element\Template
{
    protected $_invoice;

    protected $_registry;

    protected $_scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context   $context,
        \Magento\Framework\Registry                        $registry,
        array                                              $data = []
    )
    {
        $this->_registry = $registry;
        $this->_scopeConfig = $scopeConfig;

        parent::__construct($context, $data);
    }

    public function getAnchorLink()
    {
        $orderId = $this->getInvoice()->getOrder()->getIncrementId();

        $filesystem = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\Filesystem');
        $mediaDirectory = $filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);

        $shopName = $this->_scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $orderDate = date('Ymd');

        $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
        $fileName = str_replace("[order_number]", $orderId, $fileName);
        $fileName = str_replace("[order_date]", $orderDate, $fileName);
        $fileName = str_replace("[shop_name]", $shopName, $fileName);
        $fileName = str_replace(" ", "-", $fileName);

        $fileExists = $mediaDirectory->isExist($fileName);

        if ($fileExists) {
            $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');
            $url = $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $fileName;

            return '<a href="' . $url . '#my-anchor" target="_blank">' . __('Open QR Invoice') . '</a>';
        }

        return '';
    }

    public function getInvoice()
    {
        if (!$this->_invoice) {
            $this->_invoice = $this->_registry->registry('current_invoice');
        }

        return $this->_invoice;
    }

    private function getScopeConfigValue($key)
    {
        return $this->_scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
